PokeMMO Installer for GNU/Linux
===============================

This program will download and install the PokeMMO client in a user's home directory.

PokeMMO client is an online multiplayer emulator created by fans, allowing you to play
the saga of pokemon games, where you can interact with other trainers by talking,
fighting and swapping the monsters you encounter.

**Register**
------------

To play this game, you need to make a free registration on the official website.

- https://pokemmo.eu/account

The permitted usage of the PokeMMO game client is defined by a non-free license.
The content of this license can be found in the game client after an account's
first login or at https://pokemmo.eu/tos.

**Installation dependency: Debian/Ubuntu/GNU/Linux**
----------------------------------------------------

    # apt install default-jre make openssl zenity wget

When all these dependencies have installed, simply run the script.

Next you need to compile this release.

**Compilation**
---------------

To build game, do from the source directory: (Requires root access for compilation)

    # make install

Once completed, it will appear in the application menu or run the created binary:

    $ pokemmo-installer

To make the removal, within the compiled directory, execute this command:
    
    # make uninstall

**Important Request**
---------------------

This emulator has an important requirement, however: you need to have the ROMs of the games
in question available for the game to work, it is absolutely necessary to have:

 * Current Required Compatible ROMS: Black/White 1 (NDS)
 * Current Optional Content Compatible ROMS: Fire Red, Emerald (GBA)
 * Current Optional Visuals Compatible ROMS: HeartGold, SoulSilver (NDS)

You need to enter in the hidden personal directory (**$HOME/.local/share/pokemmo/roms**)
the roms of the games in the versions **GameBoy Advance** and **Nintendo DS**.

> **You must have the legal right to use that rom. We will not supply you with
> the roms, or help you find them as they are copyrighted.**

**License**
-----------
The program is published under GPLv3 with OpenSSL exception,
the full license can be found in the LICENSE file.

> This program is free software: you can redistribute it and/or modify
> it under the terms of the GNU General Public License as published by
> the Free Software Foundation, either version 3 of the License, or
> (at your option) any later version.

- (c) Copyright 2012-2020 **PokeMMO Game Client** PokeMMO.eu <linux@pokemmo.eu>
- (c) Copyright 2017-2020 **PokeMMO Installer** Carlos Donizete Froes [a.k.a coringao]
